# GTJ High School First Robotics Team 5841 

Our code is mostly visible to all.

We will eventually add more information to this readme and repository on how you could join (If you are a student at TJ). For now, simply make an issue in this repository.

... some links to our code: 
2024 spring code: https://gitlab.com/robotics-5841/5841WolfGang2024Spring (.git)
2023 fall code: https://gitlab.com/robotics-5841/5841Bart2023Fall (.git)
2023 spring code: https://gitlab.com/robotics-5841/5841Patrick2023 (.git)
2022 code: https://gitlab.com/robotics-5841/Robotics2022Two (.git)


installers: https://gitlab.com/robotics-5841/installers-alt
